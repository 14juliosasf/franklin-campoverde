package ec.telconet.teorica.algoritmo.franklin.campoverde;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de elementos del arreglo: ");
        int n = scanner.nextInt();
        int[] miArreglo = new int[n];

        System.out.println("Ingrese los elementos del arreglo (numeros enteros del 1 al 9):");
        for (int i = 0; i < n; i++) {
            System.out.print("Elemento " + (i + 1) + ": ");
            miArreglo[i] = scanner.nextInt();
        }

        scanner.close();

        int longitudMaxima = 0;
        int numeroMaximo = 0;
        int longitudActual = 0;

        for (int i = 0; i < miArreglo.length - 1; i++) {
            if (miArreglo[i] == miArreglo[i + 1]) {
                longitudActual++;
            } else {
                longitudActual = 0;
            }

            if (longitudActual > longitudMaxima) {
                longitudMaxima = longitudActual;
                numeroMaximo = miArreglo[i];
            }
        }

        System.out.println("Recurrencias: " + (longitudMaxima + 1));
        System.out.println("Numero: " + numeroMaximo);
    }
}

