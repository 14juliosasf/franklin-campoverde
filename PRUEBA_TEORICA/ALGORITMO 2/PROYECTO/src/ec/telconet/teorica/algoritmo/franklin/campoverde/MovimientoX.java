package ec.telconet.teorica.algoritmo.franklin.campoverde;

import java.util.Arrays;

public class MovimientoX {
    private static final int TAMANIO_TABLERO = 4;
    private static final char SIMBOLO_X = 'X';
    private static final char SIMBOLO_VACIO = 'O';

    private int x;
    private int y;

    public MovimientoX() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * Mueve la X segun los desplazamientos proporcionados.
     *
     * @param desplazamientoX La cantidad de unidades a moverse en el eje horizontal.
     * @param desplazamientoY La cantidad de unidades a moverse en el eje vertical.
     */
    public void mover(int desplazamientoX, int desplazamientoY) {
        int nuevaX = Math.max(0, Math.min(TAMANIO_TABLERO - 1, x + desplazamientoX));
        int nuevaY = Math.max(0, Math.min(TAMANIO_TABLERO - 1, y + desplazamientoY));
        x = nuevaX;
        y = nuevaY;
    }

    /**
     * Genera una representacion visual del tablero con la posicion de la X.
     *
     * @return El tablero como una cadena de caracteres.
     */
    public String toString() {
        char[][] tablero = new char[TAMANIO_TABLERO][TAMANIO_TABLERO];
        for (char[] fila : tablero) {
            Arrays.fill(fila, SIMBOLO_VACIO);
        }
        tablero[y][x] = SIMBOLO_X;

        StringBuilder sb = new StringBuilder();
        for (char[] fila : tablero) {
            for (char celda : fila) {
                sb.append(celda);
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        int[] miArreglo = {1, 2, -1, 1, 0, 1, 2, -1, -1, -2};

        MovimientoX movimientoX = new MovimientoX();
        for (int i = 0; i < miArreglo.length; i += 2) {
            int desplazamientoX = miArreglo[i];
            int desplazamientoY = miArreglo[i + 1];
            movimientoX.mover(desplazamientoX, desplazamientoY);
        }

        System.out.println(movimientoX);
    }
}

