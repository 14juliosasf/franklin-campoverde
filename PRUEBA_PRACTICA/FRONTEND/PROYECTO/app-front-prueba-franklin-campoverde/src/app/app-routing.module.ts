import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './pages/user/user.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { EditUserComponent } from './pages/user/edit-user/edit-user.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
    {path: 'user', component: UserComponent},
    {path: 'user/edit', component: EditUserComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
