import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User.model';
import { AuthService } from 'src/app/service/auth/auth.service';
import { TokenService } from 'src/app/service/token/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  isLogged : boolean = false;
  usuarioLogged! :User |null ;
  isAdmin: boolean = false;
  isMod: boolean = false;
  isUser: boolean = false;

  constructor(
    private tokenService:TokenService, 
    private authService: AuthService, 
    private router: Router
  ) { }
  
  ngOnInit(){
    this.isAdmin = this.tokenService.isAdmin();
    this.isMod = this.tokenService.isMod();
    this.isUser = this.tokenService.isUser();
    this.isLogged = this.tokenService.islogged();
    this.usuarioLogged = JSON.parse(this.authService.traerPersonaLogeada());
  }

  logout() {
    this.tokenService.logout();
    window.location.reload();
    this.router.navigate(['/login']);
  }

  goToUserPage() {
    this.router.navigateByUrl('/user');
  }
  
}
