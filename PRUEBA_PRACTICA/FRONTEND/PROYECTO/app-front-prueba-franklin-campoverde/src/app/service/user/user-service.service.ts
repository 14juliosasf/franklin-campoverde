import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/User.model';
import { Role } from '../../models/Role.model';

const jwt = localStorage.getItem('auth-token');
const baseUrl: string = 'http://localhost:8080';
const headers = new HttpHeaders({
  'Authorization': `Bearer ${jwt}`
});

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {

  constructor(private httpClient: HttpClient) { }
  
  listar() {
    return this.httpClient.get<User[]>(`${baseUrl}/user`,{headers});
  }
  agregar(user: User){
    return this.httpClient.post<User>(`${baseUrl}/user`, user,{headers});
  }
  eliminar(id: number) {
    return this.httpClient.put<User[]>(`${baseUrl}/user/delete/${id}`, null,{headers});
  }
  obtenerUnUser(id: number){
    return this.httpClient.get<User>(`${baseUrl}/user/${id}`,{headers});
  }
  updateUser(id:number ,user: User){
    return this.httpClient.put<User>(`${baseUrl}/user/edit/${id}`, user,{headers});
  }
  traerRoles(){
    return this.httpClient.get<Role[]>(`${baseUrl}/user/roles`,{headers});
  }
}
