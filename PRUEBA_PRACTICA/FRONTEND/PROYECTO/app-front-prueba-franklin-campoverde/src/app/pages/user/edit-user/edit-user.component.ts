import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Role } from 'src/app/models/Role.model';
import { User } from 'src/app/models/User.model';
import { UserServiceService } from 'src/app/service/user/user-service.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent {
  usuario!: User;//Usuario recuperado para editar
  form!: FormGroup;
  roles: Role[] = [];
  @Input() idUsuarioAEditar!: User;
  id = localStorage.getItem('id');

  constructor(
    private usuarioService: UserServiceService,
    private formBuilder: FormBuilder,
    private router: Router,
    // private tokenService: TokenService
  ) { }


  readonlyMode: boolean = true;

  ngOnInit() {    

      this.form = this.formBuilder.group({
        username: ['' ],
        email: ['' ],
        firstname: ['' ],
        lastname: ['' ],
        status: ['' ],
        roles: ['' ],
      });
      //Traer los roles para el select
      this.usuarioService.traerRoles().subscribe({
        next: (data: Role[]) => {
          this.roles = data;
        },
        error: (error) => { console.log(`Ocurrió un error al traer los roles ${error.status}`); },
        complete: () => { }
      });
      //Obtener el usuario y setear los valores iniciales
      this.usuarioService.obtenerUnUser(+this.id!).subscribe({
        next: (data) => {
          this.usuario = data;
        },
        error: (error) => { console.log(`Ocurrió un error al traer el usuario ${error.status}`); },
        complete: () => {
          this.form.patchValue({
            username: this.usuario.username,
            email: this.usuario.email,
            firstname: this.usuario.firstname,
            lastname: this.usuario.lastname,
            status: this.usuario.status,
            roles: this.toJson(this.usuario.roles?.[0]),
          });
        }
      });
    
  }

  volver() {
    localStorage.removeItem('id');//a mejorar
    this.router.navigate(['user']);
  }
  toJson(value: any) {
    return JSON.stringify(value);
  }
  guardar() {
    if (this.form.valid) {
      const usuario: User = {
        id: this.usuario.id,
        firstname: this.form.value.firstname,
        lastname: this.form.value.lastname,
        status: this.form.value.status,
        roles: [JSON.parse(this.form.value.roles)],
      };
      console.log(this.form.value.roles);
      this.usuarioService
        .updateUser(usuario.id!, usuario)
        .subscribe({
          next:(data) => {
            this.usuario = data;
            alert('Se actualizo con exito');
            this.router.navigate(['user']);
          },
          error: (error) => { console.log(`Ocurrió un error al actualizar el usuario ${error.status}`); },
          complete: () => {
            localStorage.removeItem('id');
           }
        });
    } else {
      alert('Debe completar todos los campos');
    }
  }
}
