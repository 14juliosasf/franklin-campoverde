import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/User.model';
import { TokenService } from 'src/app/service/token/token.service';
import { UserServiceService } from 'src/app/service/user/user-service.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent{

  usuarioSeleccionado!: User;
  usuarioIdSeleccionado!: number;
  isAdmin: boolean = false;
  isMod: boolean = false;
  page!: number ;

  constructor(
    private usuarioService: UserServiceService,
    private router: Router,
    private tokenService: TokenService
  ) {}

  obtenerUsuario(usuario: User) {
    this.usuarioSeleccionado = usuario;
  }

  obtenerUsuarioId(usuario: User) {
    localStorage.setItem('id', usuario.id!.toString());
    this.router.navigate(['user/edit']);
  }

  usuarios: User[] = [];

  ngOnInit() {
    if (this.tokenService.isAdmin() || this.tokenService.isMod()) {
      this.isAdmin = this.tokenService.isAdmin();
      this.isMod = this.tokenService.isMod();
      this.usuarioService.listar().subscribe({
        next: (data: User[]) => {
          this.usuarios = data.filter((usuario: User) => usuario.status);
        },
        error: (error) => {
          console.log(`Ocurrió un error al traer los usuarios ${error.status}`);
          this.tokenService.logout();
          window.location.replace('/login');
        },
        complete: () => {},
      });
    }else{
      this.router.navigate(['/']);
    }
  }

  delete(id: number) {
    this.usuarioService.eliminar(id).subscribe({
      next: (data: User[]) => {
        this.usuarios = data.filter((usuario: User) => usuario.status);
      },
      error: (error) => {
        console.log(`Ocurrió un error al eliminar el usuario ${error.status}`);
      },
      complete: () => {},
    });
  }

  onUserSave(usuario: User) {
    this.usuarios.push(usuario);
  }
  
}
