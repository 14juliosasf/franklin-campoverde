import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Role } from 'src/app/models/Role.model';
import { User } from 'src/app/models/User.model';
import { UserServiceService } from 'src/app/service/user/user-service.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @Output() usuarioGuardado = new EventEmitter<User>();

  form!: FormGroup;
  roles: Role[] = [];

  constructor(
    private usuarioService: UserServiceService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}
  
  toJson(value: any) {
    return JSON.stringify(value);
  }
  
  
  ngOnInit() {
    this.usuarioService.traerRoles().subscribe({
      next: (data: Role[]) => {
        this.roles = data;
      },
      error:()=>{console.log('Ocurrió un error en el servidor');},
      complete:() => {}
    });
    this.form = this.formBuilder.group({
      username: ['' ],
      email: ['' ],
      password: ['' ],
      firstname: ['' ],
      lastname: ['' ],
      status: ['' ],
      roles: ['' ],
    });
  }
  guardar() {
    if (this.form.valid) {
      const usuario: User = {
        username: this.form.value.username,
        email: this.form.value.email,
        password: this.form.value.password,
        firstname: this.form.value.firstname,
        lastname: this.form.value.lastname,
        status: this.form.value.status,
        roles: [JSON.parse(this.form.value.roles)] ,
      };
      this.usuarioService.agregar(usuario).subscribe({
        next:(data) => {
          this.usuarioGuardado.emit(data);
          alert('Usuario creado');
          this.router.navigate(['user']);
        },
        error:()=>{
          alert('Ocurrió un error en el servidor');
        },
        complete:() => {
          this.form.reset();
        }
      });
    } else {
      alert('Debe completar todos los campos');
    }
  }
  emitirEventoUsuarioGuardado(usuario: User) {
    this.usuarioGuardado.emit(usuario);
  }
}
