import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from 'src/app/models/User.model';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.css']
})
export class ModalDeleteComponent {
  @Input() idUsuarioAEliminar!: User;
  @Output() eliminarUsuario = new EventEmitter<number>();

  eliminarUsuarioConfirmado() {
    this.eliminarUsuario.emit(this.idUsuarioAEliminar.id);
  }
}
