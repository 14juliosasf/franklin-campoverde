import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/service/token/token.service';
import { User } from 'src/app/models/User.model';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  isLogged: boolean = false;
  usuarioLogged! :User |null ;
  isAdmin: boolean = false;
  isMod: boolean = false;
  isUser: boolean = false;
  
  constructor(
    private router: Router,
    private authService: AuthService,
    private tokenService: TokenService
  ) {}

  ngOnInit() {
    this.isAdmin = this.tokenService.isAdmin();
    this.isMod = this.tokenService.isMod();
    this.isUser = this.tokenService.isUser();
    this.isLogged = this.tokenService.islogged();
    this.usuarioLogged = JSON.parse(this.authService.traerPersonaLogeada());
    if (!this.tokenService.islogged()) {
      this.router.navigate(['/login']);
    }else{
      this.router.navigate(['/']);
    }
  }
}
