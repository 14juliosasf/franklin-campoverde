package ec.telconet.prueba.franklin.campoverde.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;

import ec.telconet.prueba.franklin.campoverde.model.Role;
import ec.telconet.prueba.franklin.campoverde.model.User;
import ec.telconet.prueba.franklin.campoverde.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {
    
    @Autowired
    private UserService userService;
    
    ////////////////GETS////////////////
    @GetMapping
    @PreAuthorize("hasRole('CONSULTOR') or hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public ResponseEntity<?> listar() {
        return userService.listarTodos();
    }

    @GetMapping("/roles")
    @PreAuthorize("hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public List<Role> getRoles(){
        return userService.listarAllRoles();
    } 
    
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public User getUsuarioById(@PathVariable("id") Long id ){
        return userService.listarById(id);
    }
    
    ////////////////POST////////////////
    @PostMapping
    @PreAuthorize("hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public ResponseEntity<?> ingresar(@RequestBody User transaccion){
		return userService.insertar(transaccion);
	}
    
    ////////////////PUTS////////////////
    @PutMapping("/edit/{id}")
    @PreAuthorize("hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public User actualizar(@PathVariable Long id, @RequestBody User transaccion){
    	transaccion.setId(id);
        return userService.actualizar( transaccion);
    }
    
    ////////////////DELETE////////////////
    @PutMapping("/delete/{id}")
    @PreAuthorize("hasRole('EDITOR') or hasRole('ADMINISTRADOR')")
    public List<User> eliminar(@PathVariable Long id){
        return userService.eliminar(id);
    } 

}
