package ec.telconet.prueba.franklin.campoverde.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import ec.telconet.prueba.franklin.campoverde.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	Optional<User> findByUsername(String username);
	
	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	@Query("SELECT u FROM User u WHERE u.status != 'N' ORDER BY u.id ASC")
	  public List<User> findByEstado();
	  
	@Query("SELECT r FROM User r ORDER BY r.id ASC")
	  public List<User> findAllUsers();
	
	@Modifying
	@Query("UPDATE User u SET u.status = 'N' WHERE u.id = ?1")
	  public void deleteById(Long id);

}
