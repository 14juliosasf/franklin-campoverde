package ec.telconet.prueba.franklin.campoverde.service;

import java.util.List;
//import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ec.telconet.prueba.franklin.campoverde.model.Role;
import ec.telconet.prueba.franklin.campoverde.model.User;
import ec.telconet.prueba.franklin.campoverde.payload.response.MessageResponse;
import ec.telconet.prueba.franklin.campoverde.repository.RoleRepository;
import ec.telconet.prueba.franklin.campoverde.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
    PasswordEncoder encoder;
	
	////////////// INGRESAR //////////////
	public ResponseEntity<?> insertar(User newUser){
		if(camposUnicosYnoNulos(newUser).getStatusCode().is4xxClientError()) {
			return camposUnicosYnoNulos(newUser);
		}else {
			newUser.setPassword(encoder.encode(newUser.getPassword()));
			userRepository.save(newUser);
			return ResponseEntity.ok(newUser);
		}
	}
	////////////// ACTUALIZAR //////////////
	public User actualizar(User user) {
		
		//Optional<User> optionalUsuario = userRepository.findById(user.getId());
        
        //User usuarioEditado = optionalUsuario.get();
        copiarCamposNoNulos(user);
        return userRepository.save(user);
    }
	
	////////////// LISTAR //////////////
	public ResponseEntity<?> listarTodos() {
        List<User> usuarios = userRepository.findByEstado();
        return ResponseEntity.ok(usuarios);
    }
	
	public User listarById(Long id) {
        return userRepository.findById(id).get();
    }
	
	public List<User> listarAllUsers() {
        return userRepository.findAllUsers();
    }
	
	public List<Role> listarAllRoles() {
        return roleRepository.findAllRoles();
    }
	
	////////////// REGISTRO USUARIOS //////////////
	public ResponseEntity<?> registrar(@Valid User signUpRequest) {
        
        if(camposUnicosYnoNulos(signUpRequest).getStatusCode().is4xxClientError()){
            return camposUnicosYnoNulos(signUpRequest);
        }
        Set<Role> strRoles = roleRepository.getRoleUser();
        signUpRequest.setRoles(strRoles);
        signUpRequest.setStatus("A");
        signUpRequest.setPassword(encoder.encode(signUpRequest.getPassword()));
        
        userRepository.save(signUpRequest);
        
        return ResponseEntity.ok(new MessageResponse("Usuario registrado satisfactoriamente!"));
    }
	
	////////////// ELIMINACIÓN LÓGICA //////////////
	public List<User> eliminar(Long id) {
		userRepository.deleteById(id);
        return userRepository.findByEstado();
    }
	
	////////////// VALIDACIÓN //////////////
	
	private void copiarCamposNoNulos(User fuente) {
		
		User destino = userRepository.findById(fuente.getId()).orElse(fuente);
		
		if (fuente.getUsername() == null) {
        	fuente.setUsername(destino.getUsername());
        }
        if (fuente.getFirstname() == null) {
        	fuente.setFirstname(destino.getFirstname());
        }
        if (fuente.getLastname() == null) {
        	fuente.setLastname(destino.getLastname());
        }
        if (fuente.getEmail() == null) {
        	fuente.setEmail(destino.getEmail());
        }
        if (fuente.getPassword() == null) {
        	fuente.setPassword(destino.getPassword());
        }
        if (fuente.getRoles().size() > 0) {
        	fuente.setRoles(destino.getRoles());
        }
        if (fuente.getStatus() == null) {
        	fuente.setStatus(destino.getStatus());
        }
    }
	
	private ResponseEntity<?> camposUnicosYnoNulos(User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Usuario utilizado anteriormente!"));
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Email ya utilizado anteriormente!"));
        }
        if (user.getUsername() == null || user.getEmail() == null || 
        user.getLastname() == null || user.getFirstname() == null || 
        user.getPassword() == null) {
            return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Campos vacios!"));
        }
        return ResponseEntity.ok(new MessageResponse("Usuario registrado satisfactoriamente!"));
    }
}
