package ec.telconet.prueba.franklin.campoverde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsCompPruebaFranklinCampoverdeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaFranklinCampoverdeApplication.class, args);
	}

}
