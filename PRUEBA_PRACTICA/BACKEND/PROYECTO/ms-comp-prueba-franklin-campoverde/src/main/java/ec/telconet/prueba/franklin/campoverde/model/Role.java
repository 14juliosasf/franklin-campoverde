package ec.telconet.prueba.franklin.campoverde.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "roles")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(length = 20)
	private String name;

	public Role(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Role() {
	}

}