CREATE DATABASE telconet;

CREATE TABLE public.roles (
	id serial4 NOT NULL,
	"name" varchar(20) NULL,
	CONSTRAINT roles_pkey PRIMARY KEY (id)
);

CREATE TABLE public.users (
	id serial NOT NULL,
	username varchar(30) NOT NULL,
	email varchar(50) NOT NULL,
	firstname varchar(100) NOT NULL,
	lastname varchar(100) NOT NULL,
	password varchar(250) NOT NULL,
	status varchar(1) COLLATE pg_catalog."default" NOT NULL,
	CONSTRAINT unique_email UNIQUE (email),
	CONSTRAINT unique_username UNIQUE (username),
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT estado_check CHECK (status::text = ANY (ARRAY['A'::character varying::text, 'I'::character varying::text]))
);

CREATE TABLE public.user_roles (
	user_id int8 NOT NULL,
	role_id int4 NOT NULL,
	CONSTRAINT fk_rolesid FOREIGN KEY (role_id) REFERENCES public.roles(id),
	CONSTRAINT fk_userid FOREIGN KEY (user_id) REFERENCES public.users(id),
	CONSTRAINT user_roles_pkey PRIMARY KEY (user_id, role_id)
);

INSERT INTO public.users
( username, email, firstname, lastname, password, status)
VALUES('administrador@prueba.com', 'administrador@prueba.com', 'Franklin', 'Campoverde', '$2a$10$h.79dmJt01F1d2XVa6.8sOvvM1va5gd5gGvgbSBaCEciPpEWbPsbG','A');

INSERT INTO public.roles
(id, "name")
values
(1, 'ROLE_ADMINISTRADOR'),
(2, 'ROLE_CONSULTOR'),
(3, 'ROLE_EDITOR');

INSERT INTO public.user_roles
(user_id, role_id)
VALUES(1, 1);